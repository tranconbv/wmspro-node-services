# hs node services

A simple package including the following services:

* require().logger => Gcloud logger
* require().config => config extension/wrapper providing support for seperate environment variables 


## config

based on [config](https://www.npmjs.com/package/config).

It will check environment variables first (case sensitive), in the following manner:

```bash
    export NODE_CONFIG={prop: {inner: "test"} }
    #equals
    export prop_inner=test
```

In this case the later will overwrite the first. 

This is usefull for production and docker environment for manual overwrites.