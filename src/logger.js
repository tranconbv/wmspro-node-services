const uuid = require('uuid/v4');
const Logging = require('@google-cloud/logging');
const configFallback = require('./config.ext.js').config;
const process = require('process');

const logLevels = {
    DEFAULT: 0,
    DEBUG: 1,
    INFO: 2,
    NOTICE: 3,
    WARNING: 4,
    ERROR: 5,
    CRITICAL: 6,
    ALERT: 7,
    EMERGENCY: 8
};

const dotNetLogLevels = {
    trace: logLevels.DEBUG,
    debug: logLevels.DEBUG,
    information: logLevels.INFO,
    warning: logLevels.WARNING,
    error: logLevels.ERROR,
    critical: logLevels.CRITICAL,
    none: 100
};

// Add color for easy priority detection
// color codes: https://en.wikipedia.org/wiki/ANSI_escape_code
const logLevelColors = {
    DEFAULT: '\x1b[2m', // faint
    DEBUG: '\x1b[2m', // faint
    INFO: '\x1b[37m', // white
    NOTICE: '\x1b[32m', // green
    WARNING: '\x1b[33m', // yellow
    ERROR: '\x1b[31m', // red
    CRITICAL: '\x1b[35m', // magenta
    ALERT: '\x1b[35', // magenta
    EMERGENCY: '\x1b[35' // magenta
};

// Add alias so its always the same length
const logLevelAlias = {
    DEFAULT: 'DEFA',
    DEBUG: 'DEBG',
    INFO: 'INFO',
    NOTICE: 'NOTE',
    WARNING: 'WARN',
    ERROR: 'EROR',
    CRITICAL: 'CRIT',
    ALERT: 'ALRT',
    EMERGENCY: 'EMRG'
};

class UnhandledRejectionError extends Error {
    constructor(message) {
        super(message);
        this.code = 'UNHANDLED_REJECTION';
    }
}


class UncaughtExceptionError extends Error {
    constructor(message) {
        super(message);
        this.code = 'UNCAUGHT_EXCEPTION';
    }
}

const makeLogEntryLabels = context => Object.keys(context)
    .filter(x => x !== 'direct')
    .reduce((labels, key) => {
        labels[key] = context[key]; // eslint-disable-line no-param-reassign
        return labels;
    }, {});

class Logger {
    constructor() {
        this.queue = [];
        this.sending = false;
        this.timer = 0;
    }

    /**
     * Initialize logger with config.
     * @param {?Object} config settings.Logging or null (then it will use the config.ext.js settings.Logging).
     * @param {Object} config.LogLevel
     * @param {string} config.LogLevel.Default
     * @param {string} config.ProjectId
     * @param {string} config.Name
     * @param {string} config.AppVersion
     */
    init(config = null) {
        this.config = config;
        if (!this.config) {
            this.config = {
                Name: configFallback.get('settings.Logging.Name'),
                ProjectId: configFallback.get('settings.Logging.ProjectId'),
                LogLevel: { Default: configFallback.get('settings.Logging.LogLevel.Default') },
                AppVersion: configFallback.get('settings.AppVersion') || 'default',
            };
        }
        const level = process.env.LOG_LEVEL || this.config.LogLevel.Default;
        this.level = level; // easy acces to log level
        this.levelTreshold = dotNetLogLevels[(level || '').toLowerCase()] || logLevels.DEFAULT;
        this.client = Logging({ projectId: this.config.ProjectId });

        this.log = this.client.log(this.config.Name);
    }

    debug(context, payload, httpContext) {
        return this.write('DEBUG', payload ? context : {}, payload ? payload : context, httpContext);
    }

    info(context, payload, httpContext) {
        return this.write('INFO', payload ? context : {}, payload ? payload : context, httpContext);
    }

    warning(context, payload, httpContext) {
        return this.write('WARNING', payload ? context : {}, payload ? payload : context, httpContext);
    }

    error(context, payload, httpContext) {
        return this.write('ERROR', payload ? context : {}, payload ? payload : context, httpContext);
    }

    critical(context, payload, httpContext) {
        return this.write('CRITICAL', payload ? context : {}, payload ? payload : context, httpContext);
    }

    request() {
        return (req, res, next) => {
            const correlationId = req.get('x-hs-correlation-id') || uuid();

            req.hsContext = { correlationId }; // eslint-disable-line no-param-reassign

            this.info(req.hsContext, {
                message: `${req.method} ${req.originalUrl}`,
                method: req.method,
                url: `${req.protocol}://${req.hostname}${req.originalUrl}`,
                userAgent: req.get('User-Agent'),
                remoteIp: req.ip,
                referer: req.get('Referer')
            });

            next();
        };
    }

    requestError() {
        return (err, req, res, next) => {
            const context = req.hsContext || {};
            this.error(context, err);
            next(err);
        };
    }

    write(level, context, payload, httpContext) {
        if (this.skipEntry(level)) {
            return Promise.resolve();
        }

        this.logToConsole(level, context, payload);
        if (process.env.NODE_ENV === 'development') {
            return Promise.resolve();
        }

        const metadata = {
            resource: { type: 'global', labels: { project_id: this.config.ProjectId } },
            severity: level,
            labels: makeLogEntryLabels(context)
        };

        if (httpContext) {
            metadata.httpRequest = httpContext;
        }

        return this.logToCloud(context.direct, metadata, payload);
    }

    skipEntry(level = 'DEFAULT') {
        return (logLevels[level] || 0) < this.levelTreshold;
    }

    convertToErrorReport(error, metadata) {
        const message = {
            message: error.stack,
            serviceContext: {
                service: this.config.Name,
                version: this.config.AppVersion || 'default'
            }
        };
        if (metadata.httpRequest) {
            const httpContext = metadata.httpRequest;
            message.httpRequest = {
                url: httpContext.requestUrl,
                method: httpContext.requestMethod,
                userAgent: httpContext.userAgent,
                referrer: httpContext.referer, // crazy gcloud typo
                // responseStatusCode: res.output.payload.statusCode,
                remoteIp: httpContext.remoteIp
            };
        }
        return message;
    }

    logToCloud(flushDirectly, metadata, payload) {
        if (payload instanceof Error) {
            payload = this.convertToErrorReport(payload, metadata); // eslint-disable-line no-param-reassign
        }
        const entry = this.log.entry(metadata, payload);
        this.queue.push(entry);

        if (flushDirectly) {
            clearTimeout(this.timer);
            this.sending = true;
            return this.flushLogQueue();
        }

        if (this.sending) {
            return Promise.resolve();
        }

        this.sending = true;
        this.timer = setTimeout(() => {
            this.flushLogQueue();
        }, 1000);

        return Promise.resolve();
    }

    flushLogQueue() {
        return new Promise((resolve) => {
            this.sending = false;
            if (this.queue.length === 0) {
                resolve();
                return;
            }

            this.log.write(this.queue, (err) => {
                if (err) {
                    console.error(err); // eslint-disable-line no-console
                }

                resolve();
            });

            this.queue = [];
        });
    }

    logToConsole(level, context, payload) {
        // try to get normal message for console
        let message = '';
        if (payload && typeof payload === 'string') {
            message = payload;
        } else if (payload && payload.message && typeof payload.message === 'string') {
            message = payload.message;
        }
        console.log(`${logLevelColors[level]}${logLevelAlias[level]}\x1b[0m [${this.config.Name}]`, message);
    }
    /** Registers to log on any unhandeld promise Rejection */
    registerOnUnhandledRejection() {
        process.on('unhandledRejection', (err) => {
            if (err && err instanceof Error && err.stack && err.stack.indexOf('logger.js')) {
                return; // skip so we dont land in infinite loop
            }
            this.error({ origin: 'process_unhandledRejection' }, err instanceof Error ? err : new UnhandledRejectionError((err || 'Unkown reason').toString()));
        });
    }
    registerOnUncaughtException() {
        process.on('uncaughtException', (err) => {
            if (err && err instanceof Error && err.stack && err.stack.indexOf('logger.js')) {
                return; // skip so we dont land in infinite loop
            }
            this.error({ origin: 'process_uncaughtException' }, err instanceof Error ? err : new UncaughtExceptionError((err || 'Unkown reason').toString()));
        });
    }
}

exports.logger = new Logger();
exports.Class = Logger;
exports.UnhandledRejectionError = UnhandledRejectionError;
