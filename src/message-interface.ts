//useful for reference of Stackdriver objects: https://cloud.google.com/logging/docs/reference/v2/rest/v2/LogEntry

export interface MonitoredResource {
    labels: Object;
    type: string;
}

export interface HttpRequest {
    requestMethod: string;
    requestUrl: string;
    requestSize: string;
    status: number;
    responseSize: string;
    userAgent: string;
    remoteIp: string;
    serverIp: string;
    referer: string;
    latency: string;
    cacheLookup: boolean;
    cacheHit: boolean;
    cacheValidatedWithOriginServer: boolean;
    cacheFillBytes: string;
}

export interface LogEntry {
    logName: string;
    resource: MonitoredResource;
    httpRequest: HttpRequest;
    jsonPayload: object;
}

//Error message formats: https://cloud.google.com/error-reporting/docs/formatting-error-messages

export interface ErrorHttpContext {
    method: string;
    url: string;
    userAgent: string;
    referrer: string;
    responseStatusCode: number;
    remoteIp: string
}

export interface ErrorContext {
    httpContext: ErrorHttpContext;
    user: string;
}

export interface ErrorServiceContext {
    service: string;
    version: string;
}
export interface ErrorPayload {
    /** Error.stack */
    message: string;
    serviceContext: ErrorServiceContext;    
    context: ErrorContext;
}
