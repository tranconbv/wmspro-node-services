const logger = require('./logger.js');
/* eslint-disable no-param-reassign */

exports.register = function (server, options, next) {
    server.ext('onRequest', (hapiRequest, reply) => {
        // hapi request model: https://hapijs.com/api#request-object
        const url = (hapiRequest.headers['x-forwarded-proto'] || hapiRequest.connection.info.protocol)
            + '://' + (hapiRequest.headers['host'] || hapiRequest.info.host) + hapiRequest.url.path;
        hapiRequest.headers['x-req-start'] = (new Date()).getTime();
        hapiRequest.stackDriver = {
            httpRequest: {
                requestMethod: hapiRequest.method.toUpperCase(),
                requestUrl: url,
                requestSize: hapiRequest.raw.req.client ? hapiRequest.raw.req.client.bytesRead : 0,
                status: null,
                //responseSize: null,
                userAgent: hapiRequest.headers['user-agent'],
                remoteIp: hapiRequest.info.remoteAddress,
                serverIp: hapiRequest.raw.req.connection.localAddress,
                referer: hapiRequest.info.referrer,
                latency: null,
                //protocol: `HTTP/${hapiRequest.raw.req.httpVersion}`
            },
        };

        hapiRequest.logger = hapiRequest.stackLogger  = new Proxy(logger.logger, {
            get: (target, propName) => {

                var item = target[propName];
                if(typeof item !== 'function'){
                    return;
                }
                // return a wrapper for all (log) methods
                return function() {
                    let args = [...arguments];
                    if(args.length == 1) 
                        args = [{}, args[0]];
                    args[0].correlationId = hapiRequest.id;
                    args.push(hapiRequest.stackDriver.httpContext);
                    return item.apply(target, args);
                };
            }
        });
        return reply.continue();
    });

    server.ext('onPreResponse', (hapiRequest, reply) => {
        const {
            response, path, info, method
        } = hapiRequest;
        const httpRequest = hapiRequest.stackDriver.httpRequest;
        httpRequest.status = parseInt(response.isBoom ? response.output.statusCode : response.statusCode);
        const message =  `${httpRequest.requestUrl}: ${httpRequest.requestMethod} ${path} --> ${httpRequest.status}`;
        
        var start = parseInt(hapiRequest.headers['x-req-start']);
        var end = (new Date()).getTime();
        delete hapiRequest.headers['x-req-start'];
        const elapsedInSeconds = ((end - start)/1000.0);
        const [seconds, nanos] = elapsedInSeconds.toString().split('.');
        httpRequest.latency = { seconds: parseInt(seconds), nanos: parseInt((elapsedInSeconds-parseInt(seconds))*1000000000) };

        if(!response.isBoom){
            //response.header('x-response-time', end - start)
            (httpRequest.status.toString()[0] === '4' ? logger.logger.warning : logger.logger.debug).apply(logger.logger, [
                {}, 
                {   
                    serviceContext: {
                        service: logger.logger.config.Name,
                        version: logger.logger.config.AppVersion || 'default'
                    },
                    message: message, 
                    context: {
                        httpRequest: httpRequest,
                        user: hapiRequest.auth ? `${hapiRequest.auth.strategy} token*********` : '',
                        isAuthenticated: hapiRequest.auth.isAuthenticated
                    }
                },
                httpRequest
            ]);
        } else {
            // https://www.npmjs.com/package/boom
            (httpRequest.status.toString()[0] === '4' ? logger.logger.warning : logger.logger.error).apply(logger.logger, [
                { origin: 'hapi_onPreResponse' },
                {   
                    serviceContext: {
                        service: logger.logger.config.Name,
                        version: logger.logger.config.AppVersion || 'default'
                    },
                    message: response.stack || message, 
                    context: {
                        httpRequest: httpRequest,
                        user: hapiRequest.auth ? `${hapiRequest.auth.strategy} token*********` : '',
                        isAuthenticated: hapiRequest.auth.isAuthenticated
                    }
                },
                httpRequest
            ]);
        }
        return reply.continue();
    });
    next();
};

exports.register.attributes = {
    name: 'HapiStackDriverLogger',
    version: '1.0.0'
};
