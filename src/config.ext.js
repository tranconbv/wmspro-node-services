const config = require('config');
const process = require('process');

const SEPARATOR = '_';
const SETTINGS_KEY_PREFIXES = ['settings_', 'imports_'];

/**
 * A factory function for the config class,
 * which first extends config with environment variables.
 * @param {any} cfg node-config interface
 * @param {any} [env={}] process.env
*/
function configFactory(cfg, env = {}) {
    const util = cfg.util;
    const data = {};

    // This dictionary contains all keys.
    // Turn flat context env into structure.
    Object.keys(env)
        .filter(key => SETTINGS_KEY_PREFIXES.some(x => key.startsWith(x)))
        .forEach((key) => {
            const parts = key.split(SEPARATOR);
            parts.shift(); // ignore prefix
            const last = parts.pop();

            let context = data;
            parts.forEach((part) => {
                if (typeof (context[part]) !== 'object') {
                    context[part] = {};
                }

                context = context[part];
            });

            context[last] = env[key];
        });

    util.extendDeep(cfg, data);


    return cfg;
}

// only way to reach prototype of instance, also works in nodejs
config.__proto__.getSafe = function (property, defaultValue) { // eslint-disable-line
    return this.has(property)
        ? this.get(property)
        : defaultValue;
};

exports.config = configFactory(config, process.env);
exports.factory = configFactory;
